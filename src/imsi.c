/*
 * Copyright (C) 2017 Denis 'GNUtoo' Carikli <GNUtoo@no-log.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <pcre.h>
#include <string.h>
#include "common.h"

/* 1: find first line, mark it: Needs to pass the end of line.
 * 2: veryfy next line, if not goto 1 from after line 1: Needs to pass previous end of line + its end of line.
 * same until end line
 * re-do it until EOF
 */

/* 
 *  - First find a line with "xmm626_sec_modem_fmt_recv: Received FMT message"
 *  - Then on the *next line*, find a line with (pcre):
 *  "xmm626_sec_modem_fmt_recv: Message: aseq=0x\d\+, command=IPC_MISC_ME_IMSI, type=IPC_TYPE_NOTI, size=\d\+
 */

static enum match_state {
	MATCH_NOT_FOUND,
	FOUND_FIRST_LINE_RECEIVED_MESSAGE,
	FOUND_SECOND_LINE_COMMAND_IPC_MISC_ME_IMSI,
	FOUND_THIRD_LINE_IPC_FMT_DATA_HEADER,
	FOUND_FOURTH_LINE_IPC_FMT_DATA,
	MATCH_FOUND,
};

struct matchfunc_params {
	char* buffer;
	size_t buffsize;
	char* match_start;
	char* endline;
};

struct match_state_funcs {
	enum match_state start_state;
	int (*transition_function)(struct matchfunc_params* params);
};

int find_first_line_received_message(struct matchfunc_params* params)
{
	char* match_start;
	pcre* re;
	char* first_line = "^D/RIL-IPC \\( \\d+\\): xmm626_sec_modem_fmt_recv: " \
		"Received FMT message$";
	char* line_start;
	int ret = 0;

	if (find_text(params->buffer, params->buffsize, "D/RIL-IPC", &match_start))
		HANDLE_ERROR("find_text:", -ENOENT);

	assert (match_start > params->buffer);
	assert (match_start[-1] == '\n');

	line_start = params->buffer;
	for(;;) {
		char* line_end;
		char* end;

		line_end = strchr(line_start, '\n');

		end = (line_end) ? (line_end):  (params->buffer + params->buffsize);

		re = pcre_compile(first_line, 0, NULL, NULL, NULL);
		if (!re){
			ret = -EINVAL;
			PRINT_ERROR("pcre_compile:", ret);
			goto free_pcre;
		}

		ret = pcre_exec(re, NULL, line_start, (end - line_start - 1),
				  0, 0, NULL, 0);
		if (ret)
			printf("Match\n");
		else
			printf("No match\n");

		if (line_end)
			line_start = line_end + 1;
		else
			break;
	}

free_pcre:
	pcre_free(re);

	if (ret < 0)
		return ret; /* Error */
	else if (ret == 0)
		return MATCH_NOT_FOUND;
	else
		return FOUND_FIRST_LINE_RECEIVED_MESSAGE;
}

int find_second_line_received_message(struct matchfunc_params* params)
{

}
int find_third_line_ipc_fmt_data_header(struct matchfunc_params* params)
{

}

int find_fourth_line_ipc_fmt_data(struct matchfunc_params* params)
{

}
int find_fifth_ipc_fmt_data_footer(struct matchfunc_params* params)
{

}

static struct match_state_funcs match_functions[] = {
	{ MATCH_NOT_FOUND, find_first_line_received_message },
	{ FOUND_FIRST_LINE_RECEIVED_MESSAGE, find_second_line_received_message },
	{ FOUND_SECOND_LINE_COMMAND_IPC_MISC_ME_IMSI, find_third_line_ipc_fmt_data_header },
	{ FOUND_THIRD_LINE_IPC_FMT_DATA_HEADER, find_fourth_line_ipc_fmt_data },
	{ FOUND_FOURTH_LINE_IPC_FMT_DATA, find_fifth_ipc_fmt_data_footer},
};

int mask_imsi(char* buffer, size_t buffsize)
{
	struct matchfunc_params params;
	int (*transition_function)(struct matchfunc_params* params);
	int state;

	params.buffer = buffer;
	params.buffsize = buffsize;

	state = MATCH_NOT_FOUND;
	transition_function = match_functions[state].transition_function;

	/* TODO: bail out if the first line is not found */
	while(transition_function != MATCH_FOUND) {
		state = transition_function(&params);
		if (state < 0) {
			fprintf(stderr, "%s: Error %d: %s\n", __FUNCTION__,
				state, strerror(state));
			break;
		} else {
			transition_function = match_functions[state].transition_function;
		}
	}

	return 0;
}
