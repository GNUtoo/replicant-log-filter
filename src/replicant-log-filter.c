/*
 * Copyright (C) 2017 Denis 'GNUtoo' Carikli <GNUtoo@no-log.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "common.h"

#define OPTION_MASK_OPERATOR	(1<<0)
#define OPTION_MASK_IMSI	(1<<1)

static void print_log(char* buffer, size_t buffsize)
{
	/* TODO: Add colors */
	fprintf(stdout, "%s\n", buffer);
}

static int filter_log(char* buffer, size_t buffsize, unsigned options)
{
	int err = 0;

	if (options & OPTION_MASK_OPERATOR) {
		err = mask_operator(buffer, buffsize);
		HANDLE_ERROR("mask_operator:", err);
	}

	if (options & OPTION_MASK_IMSI) {
		err = mask_imsi(buffer, buffsize);
		HANDLE_ERROR("mask_imsi:", err);
	}

	return 0;
}

static int follow_link(int fd, char* link_path, off_t linksize)
{
	char* destination;
	struct stat sb;
	int ret;

	destination = calloc(1, linksize + 1);
	HANDLE_ERROR_ERRNO_CONDITION( "calloc:", destination, NULL);

	ret = readlink(link_path, destination, linksize);
	HANDLE_ERROR_GOTO("readlink:", ret, free_destination);

	ret = open(destination, O_RDONLY);
	HANDLE_ERROR_GOTO( "open:", ret, free_destination);
	fd = ret;

	ret = fstat(fd, &sb);
	HANDLE_ERROR_GOTO("fstat:", ret, free_destination);

	if (S_ISLNK(sb.st_mode))
		return -EAGAIN;
	else
		ret = fd;

free_destination:
	free(destination);
	return ret;
}

static int usage(char* progname)
{
	fprintf(stderr, "Usage: %s [--mask-operator] [--mask-imsi] "
	       "</path/to/log-file>\n", progname);
	return -1;
}

int main(int argc, char** argv)
{
	struct stat sb;
	void* filebuf;
	int fd, err;
	char c;
	unsigned options = 0;

	while (1) {
		int this_option_optind = optind ? optind : 1;
		int option_index = 0;
		static struct option long_options[] = {
			{"mask-operator",	no_argument,	0, OPTION_MASK_OPERATOR },
			{"mask-imsi",		no_argument,	0, OPTION_MASK_IMSI },
			{0,			0,		0, 0 }
		};

		c = getopt_long(argc, argv, "", long_options, &option_index);
		if (c == -1)
			break;

		switch (c) {
		case OPTION_MASK_OPERATOR:
		case OPTION_MASK_IMSI:
			options |= c;
			continue;
		case '?':
		default:
			return usage(argv[0]);
		}
	}

	if ( optind + 1 != argc) {
		return usage(argv[0]);
	}

	fd = open(argv[optind], O_RDONLY);
	HANDLE_ERROR_ERRNO_CONDITION("open:", fd, -1);

	err = fstat(fd, &sb);
	HANDLE_ERROR_ERRNO_CONDITION("fstat:", err, -1);

	if (S_ISLNK(sb.st_mode)) {
		/* We need the link target file size */
		do {
			fd = follow_link(fd, argv[optind], sb.st_size);
		} while (fd != -EAGAIN);
	}

	filebuf = mmap(NULL, sb.st_size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd,
		       0);
	HANDLE_ERROR_ERRNO_CONDITION("mmap:", filebuf, -1);

	filter_log(filebuf, sb.st_size, options);

	print_log(filebuf, sb.st_size);
}
