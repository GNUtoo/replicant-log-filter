/*
 * Copyright (C) 2017 Denis 'GNUtoo' Carikli <GNUtoo@no-log.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REPLICANT_LOG_FILTER_H
#define REPLICANT_LOG_FILTER_H

#include <stdio.h>
#include <sys/types.h>

#define PRINT_ERROR(msg, err)						\
	fprintf(stderr, "%s: %s %s\n", __FUNCTION__, msg,		\
		strerror((int)err));					\


#define HANDLE_ERROR_ERRNO_CONDITION(msg, err, errval)			\
	if ((int) err == errval) {					\
		err = errno;						\
		PRINT_ERROR(msg, err);					\
		return -(int)err;					\
	}								\

#define HANDLE_ERROR(msg, err)						\
	if ((int) err < 0) {						\
		PRINT_ERROR(msg, err);					\
		return -(int)err;					\
	}								\

#define HANDLE_ERROR_GOTO(msg, err, label)				\
	if ((int) err == -1) {						\
		err = errno;						\
		PRINT_ERROR(msg, err);					\
		goto label;						\
	}								\

int find_text(char* buffer, size_t buffsize, char* text, char** match_start);

int mask_imsi(char* buffer, size_t buffsize);
int mask_operator(char* buffer, size_t buffsize);

#endif /* REPLICANT_LOG_FILTER_H */
